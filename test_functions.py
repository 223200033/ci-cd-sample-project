import unittest

import functions

class TestFunctions(unittest.TestCase):
  def test_add(self):
    # integers
    self.assertEqual(functions.add(1, 2), 3)
    self.assertEqual(functions.add(-2, 4), 2)
    self.assertEqual(functions.add(0, 10), 10)

    # floats
    self.assertAlmostEqual(functions.add(1.2, 2.6), 3.8)
    self.assertAlmostEqual(functions.add(-2.3, 4.9), 2.6)
    self.assertAlmostEqual(functions.add(0.1, 10.0), 10.1)

  def test_subtract(self):
    # integers
    self.assertEqual(functions.subtract(1, 2), -1)
    self.assertEqual(functions.subtract(-2, 4), -6)
    self.assertEqual(functions.subtract(0, 10), -10)

    # floats
    self.assertAlmostEqual(functions.subtract(1.2, 2.6), -1.4)
    self.assertAlmostEqual(functions.subtract(-2.3, 4.9), -7.2)
    self.assertAlmostEqual(functions.subtract(0.1, 10.0), -9.9)

  def test_multiply(self):
    # integers
    self.assertEqual(functions.multiply(1, 2), 2)
    self.assertEqual(functions.multiply(-2, 4), -8)
    self.assertEqual(functions.multiply(0, 10), 0)

    # floats
    self.assertAlmostEqual(functions.multiply(1.2, 2.6), 3.12)
    self.assertAlmostEqual(functions.multiply(-2.3, 4.9), -11.27)
    self.assertAlmostEqual(functions.multiply(0.1, 10.0), 1.0)

  def test_divide(self):
    # integers
    self.assertEqual(functions.divide(1, 2), 0.5)
    self.assertEqual(functions.divide(-2, 4), -0.5)
    self.assertEqual(functions.divide(0, 10), 0)

    # floats
    self.assertAlmostEqual(functions.divide(1.2, 2.6), 0.4615, 4)
    self.assertAlmostEqual(functions.divide(-2.3, 4.9), -0.4694, 4)
    self.assertAlmostEqual(functions.divide(0.1, 10.0), 0.01)

if __name__ == '__main__':
  unittest.main()
