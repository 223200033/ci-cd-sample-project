# "Le Calculator": CI/CD sample project

A simple demo project to show how to configure a Gitlab CI/CD linked to an Heroku deployment to share a Python Flask app. The app is available over here:

<strong><a href="" target="_blank">https://ci-cd-flask-heroku-sample.herokuapp.com/</a></strong>

This project contains:

- a simple Flask server (in `server.py`) that uses some basic POST routes and [Jinja templates](https://flask.palletsprojects.com/en/1.1.x/templating/) to create a mini-calculator online app
- a set of operations (in `functions.py`) that the server runs and that can be tested thanks to [unit(ary) tests](https://en.wikipedia.org/wiki/Unit_testing)
- a `templates/` directory containing the various HTML/Jinja templates that are used to build the actual web pages
- a `.gitlab-ci.yml` file to describe a CI/CD pipeline for Gitlab (with a deployment to [Heroku](https://www.heroku.com/))
- various config/auxiliary files:
  - the `requirements.txt` to specify which Python libs needs to be installed for this project to work
  - the `Procfile` to specify which command to run on Heroku
  - the `.gitignore` to specify which files to ignore in the versioning
